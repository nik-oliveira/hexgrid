var credentials = require("./credentials");
const AWS = require("aws-sdk");
var AmazonCognitoIdentity = require("amazon-cognito-identity-js");

var userPool = new AmazonCognitoIdentity.CognitoUserPool(credentials.cognito);

module.exports.list = async (params) => {
    AWS.config.update(credentials.iam);
    var CognitoIdentityServiceProvider = AWS.CognitoIdentityServiceProvider;
    var client = new CognitoIdentityServiceProvider({ apiVersion: '2016-04-19', region: 'us-east-1' });

    var params = {
        UserPoolId: credentials.cognito.UserPoolId,
        Filter: '',
        Limit: 0,
    };
    
    if(params.sub) {
        params.Filter = 'sub="' + params.sub + '"';
    }

    return await new Promise((result) => {
        
        client.listUsers(params, function(err, data) {
            if(data) {
                result(data.Users)
            }
            if(err) {
                throw err;
            }
        })

    })
}

module.exports.new = async (params) => {

    var dataEmail = {
        Name : 'email',
        Value : params.email
    };
    var dataName = {
        Name : 'name',
        Value : params.nome
    };
    var dataBirthdate = {
        Name : 'birthdate',
        Value : params.birthdate
    };

    var attributeList = [];
    attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail));
    attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute(dataName));
    attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute(dataBirthdate));

    return await new Promise((resolve) => {
        userPool.signUp(params.username, params.password, attributeList, null, function(err, result){
            if (err) {
                console.log(err);
                resolve(err);
            }

            resolve(userPool.getClientId())
        });
    });

}

module.exports.logar = async (params) => {

    var authenticationData = {
        Username : params.username,
        Password : params.password,
    };
    var userData = {
        Username : params.username,
        Pool : userPool
    };
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
 
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    return await new Promise((resolve) => {
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log("aqui")
                resolve(result.getAccessToken().getJwtToken());
            },
    
            onFailure: function(err) {
                resolve(err);
            },
            mfaRequired: function(codeDeliveryDetails) {
                console.log("MFA Required");
                resolve("MFA not implemented");
            }
        });
    });

}
