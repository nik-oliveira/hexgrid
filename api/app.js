var express = require('express');
var app = express();
var user = require("./user-aws/user");

app.get("/", function(req, res, next) {
    res.end("Use the endpoints: <br> <ul><li>user/new</li><li>user/list</li><li>user/logar</li></ul>");
})

app.get("/user/list", async function(req, res, next) {
    let result = await user.list(req.query);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(result);
    
})
app.get("/user/new", async function(req, res, next) {
    let result = await user.new(req.query);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(result);
})
app.get("/user/logar", async function(req, res, next) {
    let result = await user.logar(req.query);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(result);
})

app.listen(8000);